<?php

namespace PMP\Plugins\PayPal\Checkout\Types;

use PMP\Plugins\PayPal\Checkout\Types\BaseType;

/**
 * SellerProtectionType
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class SellerProtectionType extends BaseType{

    /**
     * @var string
     */
    var $status;

    /**
     * @var \Phalcon\Config
     */
    var $dispute_categories;


}
