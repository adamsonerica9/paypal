<?php

namespace PMP\Plugins\PayPal\Checkout\Types;

use PMP\Plugins\PayPal\Checkout\Types\BaseType;

/**
 * PaymentsType 
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class PaymentsType extends BaseType {

    /**
     * @var \PMP\Plugins\PayPal\Checkout\Types\CapturesType
     */
    var $captures;

}
